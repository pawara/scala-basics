package com.scalatest.scala

import scala.math.log10
import java.io.{BufferedWriter, File, FileWriter, PrintWriter}
import scala.io.Source


object Main {

  def main(args: Array[String]): Unit = {
    println("Welcome to Scala Learning");

    //    Function returns a value
    def getSum(num1: Int = 1, num2: Int = 2): Int = {
      return num1 + num2
    }

    //    Function not return a value - Procedures in scala
    def sayHi(): Unit = {
      println("Hi how are you")
    }

    //    Function with variable number of arguments
    def getSumArray(args: Int*): Int = {
      var sum: Int = 0

      for (num <- args) {
        sum += num
      }
      sum //without return statement it will return sum
    }

    sayHi()
    print("5 + 4 = " + getSum(5, 4))
    print("\nSum array = " + getSumArray(5, 4, 6))

    //    Objects from custome classes
    val rover = new Animal
    rover.setName("Rover")
    rover.setSound("Woof")
    println("Rover String: %s".format(rover.toString()))


    val whisker = new Animal("Whiskers", "Meow")
    println("Whiskers String: %s".format(whisker.toString()))

    val spike = new Dog("Spike", "Woof", "Grrrr")
    println("Spike String: %s".format(spike.toString()))

    val fang = new Wolf("Fang")
    fang.moveSpeed = 36.0
    println("Fang String: %s".format(fang.move))

    val superman = new Superhero("superman")
    println(superman.fly)
    println(superman.hitByBullet)
    println(superman.ricochet(2500))

    /*
        Function variables and passing
        -------------------------------
        functions can pass just like other variables
    */
    val log10Func = log10 _ // _ uses to indicate it is not any other variable other than the function
    println(log10Func(1000))

    //    Log 10 calculation for a List
    List(1000.0, 10000.0).map(log10Func).foreach(println)

    List(1, 2, 3, 4, 5).map((x: Int) => x * 50).foreach((println))

    List(1, 2, 3, 4, 5).filter(_ % 2 == 0).foreach((println))

    //    Pass a function
    def times3(num: Int) = num * 3

    def times4(num: Int) = num * 4

    def multIt(func: (Int) => Double, num: Int) = {
      func(num)
    }

    printf("4 * 100 = %.1f\n", multIt(times4, 100))

    //    function declaration as a variable
    val divisorVal = 5
    val divisor5 = (num: Double) => num / divisorVal
    println("5/5 = " + divisor5(5.0))

    /*
    File - I/O
    ------------
    */
    //    Print Writer
    val writer = new PrintWriter("test.txt")
    writer.write("Just some random test\nSome more random text.")
    writer.close()

    //    File Writer
    //    val file = new File("test.txt")
    //    val writer = new BufferedWriter(new FileWriter(file))
    //    writer.write("Just some random test\nSome more random text.")
    //    writer.close()

    val textFromFile = Source.fromFile("test.txt", "UTF-8")
    val lineIterator = textFromFile.getLines

    for (line <- lineIterator)
      println(line)

    textFromFile.close()

    /*
    Exception Handling
    -------------------
    */
    def divideNums(num1: Int, num2: Int) = try {
      (num1 / num2)
    }
    catch {
      case ex: java.lang.ArithmeticException => "Can't divide by zero"
    }
    finally {
      // clean up after exception
    }

    println("3 / 0 = " + divideNums(3, 0))
  }

  /*
  Data Structures
  -----------------
  1. Array - fixed number of characters
  --------------------------------------
  val favNums = new Array[int](20)
  val friends = Array("Bob", "Tom")

  for (j<-0 to (favNums.length - 1))
  {
    favNums(j) = j
    println(favNums(j))
  }

  Using yield creates a new Array
  ----------------------------------
  val favNumsTimes2 = for(num <- favNums) yield 2 * num
  favNumsTimes2.foreach(println)

  val favNumsDiv4 = for(num <- favNums if num % 4 == 0) yield num
  favNumsDiv4.foreach(println)

  Sum, Min, Max
  --------------
  println(favNums.sum)
  println(favNums.min)
  println(favNums.max)

  Sort
  ----
  val sortedNums = favNums.sortWith(_>_)

  2. ArrayBuffer - Variable number of characters
  -----------------------------------------------
  val friends2 = ArrayBuffer[String]()
  friends2.insert(0, "Phil")
  friends2 += "Mark"
  friends2 ++= Array("Bob", "Tom")
  friends2.insert(1, "Mark", "Bob", "Tom")
  friends2.remove(1, 2)

  for (friend <- friends2)
    println(friend)

  3. Multi Dimensional Array
  ---------------------------
    var multTable = Array.ofDim[Int](10,10)

    for ( i<- 0 to 9){
      for ( j<- 0 to 9)
      {
        multTable(i)(j) = i * j
      }
    }

    for ( i<- 0 to 9){
      for ( j<- 0 to 9)
      {
        println("%d : %d = %d\n", i, j, multTable (i)(j))
      }
    }

  4. Maps
  -------
    A.) Imutable

    val employees = Map("Manager" -> "Bob Smith", "Secretary" -> "Sue Smith") // this is imutable. can't change values

    if (employees.contains("Manager"))
      printf("Manager : %s\n", employees("Manager"))

    B.) Mutable
      val customers = collection.mutable.Map ( 100 -> "Paul Smith", 101 -> "Sally Smith")

    printf("Cust 1 : %s\n", customers(100))

    customers(100) = "Tom Marks" // mutable

    customers(102) = "Megan Swift" // mutable


    traversal
    ----------
    for ((k, v) <- customers)
      printf ( "%d : %s ", k, v)

  5. Tuples (Imutable)
  ---------------------
  //    Hold numerous different type values
  //    Imutable

  var tupleMarge = (103, "Marge Simpson", 10.25)

  printf("%s owes us $%.2f\n", tupleMarge._2, tupleMarge._3

  println(tupleMarge.toString())

  */


  /*
  Classes
  -------
  No static data, static methods in scala like in Java for example
  */
  class Animal(var name: String, var sound: String) /*default constructor*/ {
    this.setName(name)
    val id = Animal.newIdNum

    //    Getters
    def getName(): String = name

    def getSound(): String = sound

    //    Setters
    def setName(name: String): Unit = {
      this.name = name
    }

    def setSound(sound: String): Unit = {
      this.sound = sound
    }

    //    Other Constructurs
    def this(name: String) /*other constructor*/ {
      this("No Name", "No Sound")
      this.setName(name)
    }

    def this() /*other constructor*/ {
      this("No Name", "No Sound")
    }

    //    Method overriding - toString() method
    override def toString(): String = {
      return "%s with the id %d says %s".format(this.name, this.id, this.sound)
    }
  }

  /*
  Handle Static
  --------------
  A different way of creating static methods inside scala

  Create a companion object of the class where we can find static class variables and functions
  */
  object Animal {

    //    static field
    private var idNumber = 0

    //    static function
    private def newIdNum = {
      idNumber += 1; idNumber
    }
  }

  /*
  Classes - Inheritance
  ----------------------
  */
  class Dog(name: String, sound: String, var growl: String) extends Animal(name, sound) {

    //    Another constructor without growl
    def this(name: String, sound: String) {
      this("No Name", sound, "No Growl")
      this.setName(name)
    }

    //    Another constructor without growl
    def this(name: String) {
      this("No Name", "No Sound", "No Growl")
      this.setName(name)
    }

    //    Another constructor without growl
    def this() {
      this("No Name", "No Sound", "No Growl")
    }

    //    Method overriding - toString() method
    override def toString(): String = {
      return "%s with the id %d says %s or %s".format(this.name, this.id, this.sound, this.growl)
    }
  }

  /*
  Classes - Abstract
  -------------------
  Can't object instant initialize (instantiated)
  */
  abstract class Mammal(val name: String) {
    //    Abstract field - field that doesn't have value assigned to it
    var moveSpeed: Double

    //    Abstract method - without a body
    def move: String
  }

  class Wolf(name: String) extends Mammal(name) {
    var moveSpeed = 35.0

    def move = "The wolf %s runs %.2f mph".format(this.name, this.moveSpeed)
  }

  /*
  Traits
  -------
  More like Java interface
  A class can extend more than one (unlike classes or abstract classes)
  Traits can provide concrete methods fields
  */
  trait Flyable {
    def fly: String
  }

  trait BulletProf {
    def hitByBullet: String

    def ricochet(startSpeed: Double): String = {
      "The bullet ricochets at a speed of %.1f ft/sec".format(startSpeed * .75)
    }
  }

  class Superhero(val name: String) extends Flyable with BulletProf {
    def fly = "%s flys through the air".format(this.name)

    override def hitByBullet: String = "The bullet bounces off of %s".format(this.name)
  }

}

